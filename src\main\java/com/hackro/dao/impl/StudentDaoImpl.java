package com.hackro.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import antlr.collections.List;

import com.hackro.dao.StudentDao;
import com.hackro.models.Student;

public class StudentDaoImpl implements StudentDao{
	
	@Autowired
	private SessionFactory session;
	
	public void add(Student student) {
		session.getCurrentSession().save(student);
		
	}

	public void edit(Student student) {
		session.getCurrentSession().update(student);
	}

	public void delete(Student student) {
		session.getCurrentSession().delete(student);
		
	}

	public Student getStudent(int studentId) {
			
		
		return (Student) session.getCurrentSession().find(String.valueOf(studentId));
	}

	public List getAllStudent() {
		
	
		return (List) session.getCurrentSession().createQuery("from student").list();
	}

}
