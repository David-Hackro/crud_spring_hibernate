package com.hackro.dao;

import antlr.collections.List;

import com.hackro.models.Student;

public interface StudentDao {
	
	public  void add(Student student);
	public  void edit(Student student);
	public  void delete(Student student);
	public  Student getStudent(int studentId);
	public List getAllStudent();

}
