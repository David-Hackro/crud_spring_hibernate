package com.hackro.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import antlr.collections.List;

import com.hackro.dao.StudentDao;
import com.hackro.models.Student;
import com.hackro.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	private StudentDao studentDao; 
	
	@Transactional
	public void add(Student student) {
	studentDao.add(student);
		
	}
	@Transactional
	public void edit(Student student) {
		studentDao.edit(student);		
	}
	@Transactional
	public void delete(Student student) {
		studentDao.delete(student);		
	}
	@Transactional
	public Student getStudent(int studentId) {
		
		return studentDao.getStudent(studentId);
	}
	@Transactional
	public List getAllStudent() {
		
		return studentDao.getAllStudent();
	}

}
