package com.hackro.service;

import antlr.collections.List;

import com.hackro.models.Student;

public interface StudentService {

	
	public  void add(Student student);
	public  void edit(Student student);
	public  void delete(Student student);
	public  Student getStudent(int studentId);
	public List getAllStudent();
}
